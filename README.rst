Copyright 2010–18 Philipp Gesang (contact_).

License: 2-clause BSD.

A PDF of the manual can be downloaded from Gitlab:
https://gitlab.com/phgsng/transliterator/wikis/doc

.. _contact: phg@phi-gamma.net

